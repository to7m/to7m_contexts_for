import re
from inspect import signature
from importlib import import_module


_module_cls = type(re)
_reserved_attr_names = {"_context_target", "_module", "_other_attrs",
                        "parent", "parents"}
_dunder_regex = re.compile('^__.*__$')


def _check_attr_name(attr_name):
    if attr_name in _reserved_attr_names:
        raise ValueError(f'attribute name "{attr_name}" is reserved')
    elif _dunder_regex.match(attr_name):
        raise ValueError(f'attribute name "{attr_name}" '
                         "appears to be a dunder method")


class CallableContextMetaclass(type):
    def __new__(mcls, callable_, module_context):
        full_name = f"{callable_.__module__}.{callable_.__qualname__}"
        cls = super().__new__(mcls, f"CallableContextFor<{full_name}>", (), {})

        if "module_context" not in signature(callable_).parameters:
            raise ValueError("signature of callable_ "
                             'lacks "module_context" parameter')

        def cls_call(self, *xargs, **kwargs):
            kwargs["module_context"] = module_context
            return callable_(*xargs, **kwargs)
        cls.__call__ = cls_call

        return cls

    def __init__(cls, *_):
        pass


class ModuleContextMetaclass(type):
    def __new__(mcls, module):
        full_name = module.__name__
        cls = super().__new__(mcls, f"ModuleContextFor<{full_name}>", (), {})

        def cls_init(self, parent=None):
            super(cls, self).__setattr__("parent", parent)
            parents = [] if parent is None else [parent, *parent.parents]
            super(cls, self).__setattr__("parents", parents)

            for attr_name, value in self._other_attrs.items():
                if isinstance(value, mcls):
                    value = value(self)

                super(cls, self).__setattr__(attr_name, value)
        super(mcls, cls).__setattr__("__init__", cls_init)

        def _global_target(attr_name):
            try:
                return getattr(module, attr_name)
            except AttributeError as err:
                try:
                    return import_module(f"{full_name}.{attr_name}")
                except ModuleNotFoundError:
                    attribute_error = err

            raise attribute_error

        def cls_getattr(self, attr_name):
            _check_attr_name(attr_name)

            global_target = _global_target(attr_name)
            context_target = self._context_target(global_target)
            super(cls, self).__setattr__(attr_name, context_target)
            return context_target
        super(mcls, cls).__setattr__("__getattr__", cls_getattr)

        def cls_setattr(self, attr_name, value):
            _check_attr_name(attr_name)

            super(cls, self).__setattr__(attr_name, value)
        super(mcls, cls).__setattr__("__setattr__", cls_setattr)

        def cls_context_target(self, global_target):
            if isinstance(global_target, _module_cls):
                submodule_context_cls = mcls(global_target)
                return submodule_context_cls(self)
            elif callable(global_target):
                if "module_context" in signature(global_target).parameters:
                    return CallableContextMetaclass(global_target, self)

            return global_target
        super(mcls, cls).__setattr__("_context_target", cls_context_target)

        super(mcls, cls).__setattr__("_module", module)
        super(mcls, cls).__setattr__("_other_attrs", {})

        return cls

    def __init__(cls, _):
        pass

    def __getattr__(cls, attr_name):
        _check_attr_name(attr_name)

        try:
            return cls._other_attrs[attr_name]
        except KeyError:
            pass

        submodule = getattr(cls._module, attr_name)
        if not isinstance(submodule, _module_cls):
            raise ValueError(
                "for classes which are _ModuleContextMetaclass instances, "
                "attributes that are accessed but not set should always "
                "correspond with module objects"
            )

        submodule_context_cls = mcls(submodule, cls)
        cls._other_attrs[attr_name] = submodule_context_cls

        return submodule_context_cls

    def __setattr__(cls, attr_name, value):
        _check_attr_name(attr_name)

        target_in_module = getattr(cls._module, attr_name)
        if isinstance(target_in_module, _module_cls):
            raise ValueError(
                "for classes which are _ModuleContextMetaclass instances, "
                "attributes that are set explicitly should never "
                "correspond with module_objects"
            )

        cls._other_attrs[attr_name] = value
